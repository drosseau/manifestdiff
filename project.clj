(defproject manifestdiff "0.1.0-SNAPSHOT"
  :description "Find a diff of two AndroidManifest files"
  :url "https://gitlab.com/drosseau/manifestdiff"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [androidmanifest/androidmanifest "0.1.0-SNAPSHOT"]
                 [org.clojure/tools.cli "0.3.7"]]
  :main manifestdiff.core
  :aot [manifestdiff.core]
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
