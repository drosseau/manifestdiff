(ns manifestdiff.core
  (:require [androidmanifest.core :as manifest])
  (:require [clojure.tools.cli :refer [parse-opts]])
  (:require [clojure.set :refer [union]])
  (:import [java.nio.file Paths Files LinkOption])
  (:gen-class))

(defn check-file
  [fname]
  (if (nil? fname)
    false
    (Files/exists
     (Paths/get fname (into-array String []))
     (into-array LinkOption []))))

(def cli-options
  [["-o" "--old MANIFEST" "Old AndroidManifest file"
    :validate [check-file "-o argument has to be a valid AndroidManifest XML file"]]
   ["-n" "--new MANIFEST", "New AndroidManifest file"
    :validate [check-file "-n argument has to be a valid AndroidManifest XML file"]]
   ["-h" "--help"]])

(defn usage
  ([opts]
   (println (:summary opts))))

(defn has-required-keys
  ([opts]
   (and (map #(not (nil? (% (:options opts)))) [:manifest :apk]))))

(defn validate-opts
  ([opts]
   (cond
     (:help (:options opts))
     (do
       (usage opts)
       (System/exit 1))
     (not (nil? (:errors opts)))
     (do
       (usage opts)
       (doseq [e (:errors opts)] (println e))
       (System/exit 1))
     (not (has-required-keys opts))
     (do
       (usage opts)
       (System/exit 1)))))

(defn in?
  [el coll]
  (some #(= el %) coll))

(defn get-eq
  "Get the element in on map that is equal to the given value according to cmp-key"
  [value cmp-key coll]
  (if (= 0 (count coll))
    nil
    (let [e (first coll)
          e-val (cmp-key e)]
      (if (= value e-val)
        e
        (get-eq value cmp-key (rest coll))))))

(defn not-in?
  [el coll]
  (not (in? el coll)))

(defn get-missing-named
  [old-coll new-coll]
  (for [e old-coll
        :when (not-in? (:name e) (map :name new-coll))]
    e))

(defn get-removed-named
  [old-coll new-coll]
  (get-missing-named old-coll new-coll))

(defn get-added-named
  [old-coll new-coll]
  (get-missing-named new-coll old-coll))

(defn get-modified-named
  [old-coll new-coll]
  (for [e old-coll
        :let [found (get-eq (:name e) :name new-coll)]
        :when (and (not (nil? e)) (not (nil? found)) (not (= e found)))]
    [e, found]))

(defn apply-func-to-list-elems
  "Apply a function to the elements of two equal length lists"
  [f coll1 coll2]
  (let [aux
        (fn
          [in-f in-coll1 in-coll2 accum]
          (let [cnt (count in-coll1)]
            (if (= 0 cnt)
              accum
              (recur
               in-f
               (rest in-coll1)
               (rest in-coll2)
               (conj  accum (in-f (first in-coll1) (first in-coll2)))))))]
    (aux f coll1 coll2 [])))

(defn apply-to-ipc-entries
  [func app1 app2]
  (let [select (juxt :services :receivers :providers :activies)
        app1-ipcs (select app1)
        app2-ipcs (select app2)]
    (apply-func-to-list-elems func (select app1) (select app2))))

(defn diff-manifests
  [old new-]
  (let
   [old-man (:manifest old)
    new-man (:manifest new-)
    old-app (:application old-man)
    new-app (:application new-man)
    new-perms (:permissions new-man)
    old-perms (:permissions old-man)
    new-uses-perms (:uses-permissions new-man)
    old-uses-perms (:uses-permissions old-man)
    new-features (:uses-features new-man)
    old-features (:uses-features old-man)
    old-app-meta (:meta-datas old-app)
    new-app-meta (:meta-datas new-app)]
    {:added (reduce union (conj
                           (apply-to-ipc-entries get-added-named old-app new-app)
                           (get-added-named old-perms new-perms)
                           (get-added-named old-app-meta new-app-meta)
                           (get-added-named old-uses-perms new-uses-perms)))
     :removed (reduce union (conj
                             (apply-to-ipc-entries get-removed-named old-app new-app)
                             (get-removed-named old-perms new-perms)
                             (get-removed-named old-app-meta new-app-meta)
                             (get-removed-named old-uses-perms new-uses-perms)))
     :modified (reduce union (apply-to-ipc-entries get-modified-named old-app new-app))}))

(defn -main
  [& args]
  (let [opts (parse-opts args cli-options)]
    (do
      (validate-opts opts)
      (let [diff (diff-manifests
                  (manifest/read-manifest (:old (:options opts)))
                  (manifest/read-manifest (:new (:options opts))))]
        (println "========== Added ==========\n")
        (doseq [e (:added diff)]
          (println (str (manifest/to-xml-string e 0) "\n\n----------\n")))
        (println "\n========== Removed ==========\n")
        (doseq [e (:removed diff)]
          (println (str (manifest/to-xml-string e 0) "\n\n----------\n")))
        (println "\n========== Modified ==========\n")
        (doseq [e (:modified diff)]
          (println "=== Previous:\n")
          (println (str (manifest/to-xml-string (first e) 0) "\n"))
          (println "=== New:\n")
          (println (str (manifest/to-xml-string (last e) 0) "\n\n----------\n\n")))))))
